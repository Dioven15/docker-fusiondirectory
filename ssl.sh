#!/bin/bash

mkdir /etc/nginx/ssl

#Required

#Change to your company details
country="GB"
state="Nottingham"
locality="Nottinghamshire"
organization="amihan.net"
organizationalunit="IT"
email="administrator@test.net"
 
#Create the request
echo "Creating CSR"
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt \
    -subj "/C=AU/ST=ssss/L=test/O=test/OU=test/CN=test/emailAddress=test"